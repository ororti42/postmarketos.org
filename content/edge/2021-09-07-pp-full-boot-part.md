title: "PinePhone /boot partition full after latest upgrade"
date: 2021-09-07
---
Some PinePhone installs with a small `/boot` partition might experience a failure when `mkinitfs` runs on upgrade:

```
==> Installing: /boot/initramfs
==> Installing: /boot/vmlinuz
==> Installing: /boot/initramfs-extra
==> Installing: /boot/sun50i-a64-pinephone-1.1.dtb
*NOT* copying file atomically (not enough free space at target): /tmp/mkinitfs402660607/allwinner/sun50i-a64-pinephone-1.1.dtb
==> Installing: /boot/sun50i-a64-pinephone-1.2.dtb
*NOT* copying file atomically (not enough free space at target): /tmp/mkinitfs402660607/allwinner/sun50i-a64-pinephone-1.2.dtb
==> Installing: /boot/uImage
*NOT* copying file atomically (not enough free space at target): /tmp/mkinitfs402660607/uImage
cp: write error: No space left on device
```

Previous versions of the PinePhone's device package in pmaports built/installed
a u-boot legacy kernel and initramfs image to /boot. This was removed in
[pma!2449](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2449),
however the old `uImage` and `uInitrd` files were not cleaned up in `/boot`,
causing `mkintifs` to fail when there is no more free space.

If your `apk upgrade` output looks like that above, where `mkinitfs` was
copying a `uImage` file, make sure that `/etc/deviceinfo` is unmodified from
what the `device-pine64-pinephone` package installs.

A proper fix is being developed, but in the meantime the following workaround
should recover things:

```
$ sudo rm /boot/uImage*
$ sudo rm /boot/uInitrd*
$ sudo apk fix -r
```

Related:

- [pmb#1228](https://gitlab.com/postmarketOS/pmaports/-/issues/1228)
- [pma!2449](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2449)
