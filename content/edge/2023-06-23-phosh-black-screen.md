title: "Phosh black screen after update"
date: 2023-06-23
---

If you had run `apk upgrade` on a Phosh installation prior to 19 June 2023, you
may have ended up with a system that starts up to a black screen on your next
reboot. This has been fixed now, so it is safe to upgrade. However, people who
were affected can run `apk fix` from ttyescape or ssh to resolve it.

This was caused by a file being moved from the Phosh package to the Squeekboard
package without the Squeekboard package declaring itself as being able to
replace files from the Phosh package.

Relevant issue: [pma#2178](https://gitlab.com/postmarketOS/pmaports/-/issues/2178)
